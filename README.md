Voting: Cats vs Dogs
=========

A simple distributed application running on True Kubernetes Service (TKS).

Run the app in Kubernetes
-------------------------

Install Voting services by push Git tag.

```sh
git tag -f v1.0.0
git push origin --tags
```

---

Architecture
-----

![Architecture diagram](architecture.png)

* A front-end web app in [Python](/vote) which lets you vote between two options.
* A [Redis](https://hub.docker.com/_/redis/) queue which collects new votes.
* A [.NET Core](/worker/src/Worker) worker which consumes votes and stores them in DB.
* A [Postgres](https://hub.docker.com/_/postgres/) database backed by a CSI volume.
* A [Node.js](/result) webapp which shows the results of the voting in real time.

---

Notes
-----

The voting application only accepts one vote per client. It does not register votes if a vote has already been submitted from a client.

This isn't an example of a properly architected perfectly designed distributed app... it's just a simple
example of the various types of pieces and languages you might see (queues, persistent data, etc), and how to
deal with them in Docker at a basic level.
